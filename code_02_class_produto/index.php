<?php

require_once("Product.php");

$db = new \PDO("mysql:host=localhost:4407;dbname=php_mysql_basico","root","");

//A classe tem uma dependência do objeto db, isso não é uma boa prática
//Isso é resolvido com injeção de dependência (dependency injection)
$produto = new Product($db);

$list = $produto->list();

var_dump($list);

