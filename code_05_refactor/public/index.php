<?php

// require_once("IConn.php");
// require_once("Conn.php");
// require_once("Product.php");
// require_once("Container.php");

require_once "../vendor/autoload.php";

use Pimple\Container;

$container = new Container();

// $container['date'] = function() {
// 	return new \DateTime;
// };

$container['date'] = $container->factory(function() {
	return new \DateTime;
});

var_dump($container['date']);

echo "</br>";
echo "</br>";
sleep(2);
var_dump($container['date']);