<?php

class Container
{
	//Um método estático pode ser usado semp recisar instânciar um objeto
	public static function getProduct()
	{
		return new Product(self::getConn());
	}

	public static function getConn()
	{
		return new Conn("mysql:host=localhost:4407;dbname=php_mysql_basico","root","");
	}
}