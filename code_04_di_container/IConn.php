<?php

interface IConn
{
	//Toda classe que implementar a minha interface IConn,
	//obrigatoriamente deverá ter a classe connect
	public function connect();
}